/**
 * Created by abzooba on 20/6/17.
 */
const Sequelize = require('sequelize');

var sequelize = new Sequelize(process.env.database, process.env.user, process.env.password, {
    host: process.env.host,
    port: parseInt(process.env.port),
    dialect: 'mysql'
});

sequelize
    .authenticate()
    .then(function (){
        console.log('Connection has been established successfully.');
    }).catch(function (err){
    console.error('Unable to connect to the database:', err);
});

module.exports = sequelize;
