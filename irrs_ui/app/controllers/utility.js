var config = require('../config');
let database = require('./database');
var winston = require('../Loggerutil');
var jwt = require('jsonwebtoken');
const _ = require('lodash');

function isString(value) {
  return typeof value === 'string';
}

function checkendpointaccesibility(accesible_endpoint, endpoint) {
  for (i in accesible_endpoint) {
    if (accesible_endpoint[i].trim() == endpoint.trim()) {
      return true;
    }
  }
  return false;
}

function checkrequest(endpoint, req) { //returns true if the required parameters and their type are correct

  switch (endpoint) {
    case 'GetSimilarTickets':
      var config_name = req.query.config;
      if (config_name.includes('qa')) {
        if (('token' in req.body) && ('type' in req.body) && ('text' in req.body) && ('url' in req.body)) {
          if (isString(req.body['token']) && isString(req.body['type']) && isString(req.body['text']) && isString(req.body['url'])) {
            return true;
          }
        }
      } else if (config_name.includes('prod')) {
        if (('token' in req.body) && ('type' in req.body) && ('text' in req.body)) {
          if (isString(req.body['token']) && isString(req.body['type']) && isString(req.body['text'])) {
            return true;
          }
        }
      }
      break;

    case 'gettoken':
      var flag = 1;
      if (('user' in req.body) && ('endpoint' in req.body)) {
        req.body.endpoint.forEach(function(value) {
          if (!isString(value) || !isString(req.body.user)) {
            flag = -1;
          }
        });
        if (flag == 1) {
          return true;
        }
      }
      break;
  }
  return false;
}


function checkEndPoint(endpoints) {

  let acceptableEndPoints = ["GetSimilarTickets"];
  let eachPoint
  let flag = false
  _.forEach(endpoints, (eachPoint) => {
    if (_.indexOf(acceptableEndPoints, _.trim(eachPoint)) != -1) {
      flag = true
      return flag
    }
  })
  return flag
}

function utility(req, res, next) {

  var endpoint = req._parsedUrl.pathname.substr(1); //give the url endpoint user wants access to
  req.query.config = req.query.config.toLowerCase()
  if (!checkrequest(endpoint, req)) { //checks if the request body has all required parameters
    res.status(400).send("Invalid Request Body");
    return;
  } else {
    next()
    return;
    var request_token = req.body.token;
    request_token = request_token.trim();    
    var query = "SELECT endpoints as endpoints FROM token  WHERE token='" + request_token + "'";
    winston.info("Query for checking peresence of token : \n" + query);
    database.executeQuery(query, function(err, result) {
      winston.info(result)
      next()
      /*if (_.isNil(result.rows) || result.rows.length === 0) return res.status(401).send({
        auth: false,
        message: 'Token does not exist.'
      });

      accesible_endpoint = result.rows[0].endpoints;
      if (err) return res.status(500).send({
        auth: false,
        message: 'Failed to authenticate token.'
      });

      accesible_endpoint = accesible_endpoint.split(",");
      if (!checkendpointaccesibility(accesible_endpoint, endpoint)) {
        return res.status(404).send({
          auth: false,
          message: 'Endpoint access not allowed.'
        });
      } else {
        next();
      }*/
    });
  }
};


function gettoken(req, res) {
  var endpoint = req.originalUrl.substr(1); //give the url endpoint user wants to access
  if (!checkrequest(endpoint, req)) {
    res.status(400).send({
      "Message": "Invalid Request body"
    });
  }

  var user = req.body.user;

  var token = jwt.sign({
    id: user
  }, config.secret);

  let endPoints = req.body.endpoint
  let endpointFlag = checkEndPoint(endPoints)

  winston.info(" flag is ", endpointFlag);
  winston.info("endpoints is ", endPoints);
  var query = "INSERT INTO token (username, endpoints, token) VALUES ( '" + user + "', '" + endPoints + "' , '" + token + "');";
  winston.info("Query for creating token : \n" + query + "\n");
  if (!endpointFlag) {
    res.status(400).send({
      "Message": "Invalid endpoint request"
    });
  } else {
    database.executeQuery(query, function(err, results) {
      if (err) {
        winston.error("error in inserting the token into mysql is " + err);
      }
    })

    res.status(200).send({
      auth: true,
      token: token
    });

  }
};


module.exports = {
  utility: utility,
  gettoken: gettoken
}
