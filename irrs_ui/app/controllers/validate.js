
function validate(body){
  var type_bool = "type" in body;     //true if type  field present in body
  // var url_bool = "url" in body;       //true if url  field present in body
  var text_bool = "text" in body;     //true if text field present in body
  var url_bool = "url" in body;
  // if( type_bool && ((body["type"] == "url" && url_bool) || (body["type"] == "text" && text_bool))){
  if( type_bool && text_bool  && url_bool){
    return true;
  }
  return false;
}
module.exports = validate;
